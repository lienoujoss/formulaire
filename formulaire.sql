-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  sam. 19 juin 2021 à 17:14
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `inchclass`
--

-- --------------------------------------------------------

--
-- Structure de la table `formulaire`
--

CREATE TABLE `formulaire` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nom_utilisateur` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sexe` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date_naissance` date NOT NULL,
  `pays` varchar(255) CHARACTER SET utf8 NOT NULL,
  `telephone` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `formulaire`
--

INSERT INTO `formulaire` (`id`, `nom`, `prenom`, `email`, `nom_utilisateur`, `password`, `sexe`, `date_naissance`, `pays`, `telephone`, `photo`) VALUES
(1, 'Lienou', 'Zedong', 'lienoujoss@gmail.com', 'Joss Lienou', 'jkjkjkj', 'homme', '2021-06-19', 'cameroun', '690-000-000', 'simplon.jpg'),
(2, 'Lienou', 'Zedong', 'lienoujoss@gmail.com', 'Joss Lienou', 'djkjkjkjq', 'homme', '2021-06-19', 'cameroun', '690000000', 'simplon.jpg'),
(3, 'Lienou', 'Zedong Zedong', 'lienoujoss@simplon.com', 'Joss Lienou', 'slklkls', 'homme', '2021-06-19', 'cameroun', '690000000', 'simplon.jpg'),
(4, 'Lienou', 'Zedong', 'lienoujoss@simplon.com', 'Joss Lienou', 'mdmkmd', 'homme', '2021-06-19', 'cameroun', '690000000', 'simplon.jpg'),
(5, 'Lienou', 'Zedong', 'lienoujoss@simplon.com', 'Joss Lienou', 'mdmkmd', 'homme', '2021-06-19', 'cameroun', '690000000', 'simplon.jpg'),
(6, 'Lienou', 'Zedong', 'lienoujoss@simplon.com', 'Joss Lienou', 'mdmkmd', 'homme', '2021-06-19', 'cameroun', '690000000', 'simplon.jpg'),
(7, 'Lienou', 'Zedong', 'lienoujoss@simplon.com', 'Joss Lienou', 'mdmkmd', 'homme', '2021-06-19', 'cameroun', '690000000', 'simplon.jpg'),
(8, 'Lienou', 'Zedong', 'lienoujoss@simplon.com', 'Joss Lienou', 'mdmkmd', 'homme', '2021-06-19', 'cameroun', '690000000', 'simplon.jpg'),
(9, 'Lienou', 'Zedong', 'lienoujoss@simplon.com', 'Joss Lienou', 'mdmkmd', 'homme', '2021-06-19', 'cameroun', '690000000', 'simplon.jpg');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `formulaire`
--
ALTER TABLE `formulaire`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `formulaire`
--
ALTER TABLE `formulaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
