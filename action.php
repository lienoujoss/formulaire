 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/action.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="css/ionicons.min.css">

  <title>Traitement</title>
 <style>
@media screen and (max-width:768px){
  .photo{
    display:none !important;
  }
  .date_naissance {
    display:none !important;
  }
  .password {
    display:none !important;
  }
  .sexe {
    display:none !important;
  }
}
@media screen and (min-width:768px) and (max-width:992px){
  .photo{
    display:none !important;
  }
  .date_naissance {
  display:none !important;
  }
  .password {
  display:none !important;
  }
  .sexe {
  display:none !important;
  }
}  

</style>

</head>

<body>
  <div class="container-fluid">
    <!-- Debut du Header -->
    <div class="row nav nav-bar header-acceuil">
      <div class="nav navbar-nav col-md-4">
        <div class="row">
          <div class="col-md-3 logo-acceuil">
            <img src="inchclassPoweredbySimplon.png" class="img-circle pull pull-left img-logo">
          </div>
          <div class="col-md-9 slogan">
            <p><span class="slogan1">L'Apprenant au centre de sa Formation</span> <br>
              <span class="slogan2">Centre de Formation Professeionnel</span>
            </p>
          </div>
        </div>
      </div>
    </div>

<?php 
  session_start(); 
?>

  <?php 
    if(isset($_POST)){
            if (isset($_FILES['photo']) AND $_FILES['photo']['error'] == 0)
            {
                if ($_FILES['photo']['size'] <= 3000000)
                {
                  $infosfichier = pathinfo($_FILES['photo']['name']);
                  $extension_upload = $infosfichier['extension'];
                  $extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
                  if (in_array($extension_upload, $extensions_autorisees))
                  {
                    move_uploaded_file($_FILES['photo']['tmp_name'], 'Images/' . basename($_FILES['photo']['name']));
                    // echo "Image recu 100 % ! "."<br> <br> ";
                    $p = $_FILES['photo']['name'];
                    // echo "<img style='height:100px; width:100px;' src='Images/".$p." ' >"."<br>";
                    $Image="<img style='height:80px; width:80px;' src='Images/".$p." ' >"."<br>";
                    // echo  $Image;

                  }
                  else
                  {
                      echo 'extention non-autorisee';
                  }
                }
                else
                {
                    echo 'Image trop Volumineuse';
                }
            }
        }
   ?>

  <?php
  $bdd=new PDO('mysql:host=localhost; dbname=inchclass', 'root', '', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

  $dataInsert=$bdd->prepare('INSERT into formulaire (nom, prenom, email, nom_utilisateur, password, sexe, date_naissance, pays, telephone, photo) VALUES(?,?,?,?,?,?,?,?,?,?)');
  $dataInsert->execute(array($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['nom_utilisateur'], $_POST['password'], $_POST['sexe'], $_POST['date_naissance'], $_POST['pays'], $_POST['telephone'], $_FILES['photo']['name']));

  $response=$bdd->query('SELECT * FROM formulaire');
  // while($donnees=$response->fetch()){
  //   echo $donnees['nom']. "---->" .$donnees['prenom']. "---->" .$donnees['email']. "---->" .$donnees['nom_utilisateur']. "---->" .$donnees['password']. "---->" .$donnees['sexe']. "---->" .$donnees['date_naissance']. "---->" .$donnees['pays']. "---->" .$donnees['ville']. "---->" .$donnees['telephone']. "---->" .$donnees['photo']. "<br>";
  //   }
  ?>



     <div class="row">
         <div class="table-responsive-md table-responsive-sm table-responsive-xl">
          <table class="table table-md table table-sm table table-xl ">
              <thead class="bg-primary"><tr>
                <td>ID</td>
                <td class="nom">Noms</td>
                <td class="prenom">Prenoms</td>
                <td class="email">Email</td>
                <td class="nom_utilisateur">Nom_Utilisateur</td>
                <td class="password">Password</td>
                <td class="sexe">Sexe</td>
                <td class="date_naissance">Date_Naissance</td>
                <td class="pays">Pays</td>
                <td class="telephone">Telephone</td>
                <td class="photo">Photo</td>
                </tr>
              </thead>;
<?php 
  while ($donnees =$response->fetch()) {
                echo '
                        <tbody class="bg-info">
                        <tr>
                          <td class="bg-primary">'.$donnees['id'].'</td>
                          <td class="nom">'.$donnees ['nom'].'</td>
                          <td class="prenom">'.$donnees ['prenom'].'</td>  
                          <td class="email">'.$donnees['email'].'</td>
                          <td class="nom_utilisateur">'.$donnees['nom_utilisateur'].'</td>
                          <td class="password">'.$donnees['password'].'</td> 
                          <td class="sexe">'.$donnees['sexe'].'</td> 
                          <td class="date_naissance">'.$donnees['date_naissance'].'</td>
                          <td class="pays">'.$donnees['pays'].'</td> 
                          <td class="telephone">'.$donnees['telephone'].'</td> 
                          <td> <img src="Images/'.$donnees['photo'].'"; style=" width: 100px; height: 100px;"</td> 
                        </tr>  
                        </tbody>';
                      
                } 
  
 ?>
          </table>;              



</body>
</html>