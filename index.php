<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Formulaire</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/action.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/ionicons.min.css">
</head>
<body>
	<div class="container-fluid">
		<!-- Debut du Header -->
		<div class="row nav nav-bar acceuil">
			<div class="nav navbar-nav col-md-4 col-sm-4 col-xs-4">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-3 ">
						<img src="inchclassLOGO-removebg-preview.png" class="img-circle pull pull-left img-logo">
					</div>
					<div class="col-md-9 col-sm-9 col-xs-9 slogan">
						<p><span class="slogan1">L'Apprenant au centre de sa Formation</span> <br>
							<span class="slogan2">Centre de Formation Professionnel</span>
						</p>
					</div>
				</div>
			</div>

			<div>
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse" >
						<span class="glyphicon glyphicon-menu-hamburger icon"></span>
				</button>
			</div>


			<div class="nav navbar-nav col-md-8 col-sm-8 col-xs-8 pull pull-right">
				
				<ul class="nav navbar-nav collapse navbar-collapse pull pull-right" style="list-style:none;">
					<li><a href="">Inscription</a></li>
					<li><a href="">Connexion</a></li>
				</ul>
			</div>
		</div>

<!-- **************************************************************************FIN DU HEADER************************************************************** -->

<!-- **************************************************************************DEBUT DU BODY************************************************************** -->
		<div class="row fond">
			<div class="col-md-7 " style="text-align:center;">
				<video class="video v" autoplay loop muted>
          			<source src="inch.mp4" type="video/mp4"/>
        		</video>
			</div>
			<div class="col-md-5 col-sm-12 col-xs-12">
				<div class="panel form1">
					<div class="panel-heading pan_head">
						<h1 class="pan_titre">Inscription</h1>
						<p class="pan_p">C'est gratuit(et ça le restera toujours)</p>
					</div>
					<div class="panel-body">
						<form id="formulaire"  method="POST" action="action.php" enctype="multipart/form-data">

							<div class="col-md-5 col-xm-5 col-xs-offset-1 col-xs-11 form2">

								
								<sub style="color:#3595ce; font-size:20px;">* </sub><span class="glyphicon glyphicon-user icon"></span>
								<label>Noms</label> 
	              				<div class="input-group">
									<input id="name" class=" form-control inpt1 " type="text" name="nom" placeholder="Votre Nom"  required="" minlength="4" 
	              					pattern="^[A-Za-z0-9_.]+${3,20}">
	              					<span class="validity input-group-addon" style="background-color: none;"></span>
	              				</div>
	              				<p class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </p>

								<sub style="color:#3595ce; font-size:20px;">* </sub><span class="glyphicon glyphicon-user icon"></span>
								<label>Prenoms</label>
								<div class="input-group">
									<input class="form-control inpt2" type="text" name="prenom" placeholder="Votre Prenom"  id="name" required="" minlength="4" 
	              					pattern="^[A-Za-z0-9_.]+${3,20}">
	              					<span class="validity input-group-addon" style="background-color: none;"></span>
              					</div>
								<p class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </p>

								<sub style="color:#3595ce; font-size:20px;">* </sub><span class="glyphicon glyphicon-envelope icon"></span>
								<label>Email</label>
								<div class="input-group">
								<input class="form-control inpt3" type="email" name="email" placeholder="Votre Email" id="name" required=""pattern="[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})">
								<span class="validity input-group-addon" style="background-color: none;"></span>
								</div>
								<p class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </p>

								<sub style="color:#3595ce; font-size:20px;">* </sub><span class="glyphicon glyphicon-user icon"></span>
								<label>Nom d'utilisateur</label>
								<div class="input-group">
									<input class="form-control inpt2" type="text" name="nom_utilisateur" placeholder="nom_utilisateur"  id="name" required="" minlength="4" 
	              					pattern="^[A-Za-z0-9_.]+${3,20}">
	              					<span class="validity input-group-addon" style="background-color: none;"></span>
              					</div>
								<p class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </p>

								<sub style="color:#3595ce; font-size:20px;">* </sub><span class="glyphicon glyphicon-lock icon"></span>
								<label>Mot de pass</label>
								<div class="input-group">
									<input class="form-control inpt5" type="password" name="password" placeholder="Votre Mot de pass" id="name"  required="">
									<span class="validity input-group-addon" style="background-color: none;"></span>
								</div>
								<p class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </p>

							</div>


							<div class="col-md-offset-1 col-md-5 col-md-pull-1 col-xm-5 col-xs-offset-1 col-xs-11 form3">
								<sub style="color:#3595ce; font-size:20px;">* </sub><span class="fa fa-transgender icon"></span>
								<label> <sub style="color: red;">* </sub>Sexe</label>
								<select class="form-control sel2" placeholder="" name="sexe" id="name" required="">
									<option selected="selected"  value="v" disabled="" >Sexe</option>
									<option class="form-control" value="femme">Femme</option>	
									<option class="form-control" value="homme">Homme</option>	
								</select>
								<p class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </p>

								<sub style="color:#3595ce; font-size:20px;">* </sub><span class="glyphicon glyphicon-time icon"></span>
								<label>Date de naissance</label>
								<div class="input-group">
									<input class="form-control inpt3" type="date" id="name" name="date_naissance" placeholder="Exple: 11 - 08 - 2021" pattern="(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/](19|20)\d\d" id="date_naissance" required="">
									<span class="validity input-group-addon" style="background-color: none;"></span>
								</div>
								<p class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </p>

								<span class="glyphicon glyphicon-flag icon"></span>
								<label>Pays</label>
								<select class="form-control sel1" placeholder="" id="name" name="pays" required="">
									<optgroup label="Afrique">
				                    <option   selected="selected" disabled=""> Choisi ton Pays d'origine </option>
				                    <option value="cameroun">Cameroun</option>
				                    <option value="tchad">Tchad</option>
				                    <option value="congo">Congo</option>
				                    <option value="gabon">Gabon</option>
				                    <option value="rwanda">Rwanda</option>
				                    <option value="RCA">RCA</option>
				                    <option value="nigeria">Nigeria</option>
				                    <option value="niger">Niger</option>
				                    <option value="egypte">Egypte</option>
				                   	</optgroup>

				                    <optgroup label="Amérique">
				                    <option value="canada">Canada</option>
				                    <option value="etats-unis">Etats-Unis</option>
				                    <option value="brazil">Brazil</option>
				                    <option value="chili">Chili</option>
				                    </optgroup>   

				                    <optgroup label="Asie">
				                    <option value="chine">Chine</option>
				                    <option value="japon">Japon</option>
				                    </optgroup>
								</select>
								<p class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </p>

							
								<sub style="color:#3595ce; font-size:20px;">* </sub><span class="glyphicon glyphicon-earphone icon"></span>
								<label>Telephone</label>
								<span style="background-color:#f1f3f2;">
				                  <select  style="border: none;" id="name">
				                    <option value="00237" selected="selected" disabled=""> +237 </option>
				                    <option value="00247"> +247 </option>
				                    <option value="00241"> +241 </option>
				                    <option value="00228"> +228 </option>
				                    <option value="00200"> +200 </option>
				                    <option value="00225"> +225 </option>
				                    <option value="00205"> +1 </option>
				                    <option value="00205"> +33 </option>
				                    <option value="00205"> +30 </option>
				                    <option value="00205"> +285 </option>
				                    <option value="00205"> +286 </option>
				                    <option value="00205"> +221 </option>
				                   	</select>
				                </span>
				                <div class="input-group">
									<input class="form-control inpt5" type="" name="telephone" placeholder="Votre Numéro de Telephone" id="name" pattern="[0-9]+" minlength="9" maxlength="20"required="">
									<span class="validity input-group-addon" style="background-color: none;"></span>
								</div>
								<p class="alert alert-danger Erreur"> Vous devez faire un choix </p>
								<span class="glyphicon glyphicon-picture icon"></span>
								<label>Photo de pofil</label>
								<input class="form-control inpt7" type="file" name="photo" accept="image/*" onchange="loadFile(event)" required="" >
								<div class="col-md-8 im" style="">
	                            <img id="pp"/>
	                      		</div>

							</div>

							<div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6 bout">
								<input  type="reset" id="reset" class="  btn btn-block btn-info bout1"> 
						
							</div>
							<div class="  col-md-4 col-xm-5 col-xs-6">
								<input  type="submit" id="submit" class=" bout btn btn-block btn-info btn-success" >
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

<!-- ************************************************************************** FIN DU BODY************************************************************** -->

<!-- **************************************************************************DEBUT DU FOOTER************************************************************** -->

		<div class="row footer">
			<div class="col-md-12">
				<div class="col-md-3 bloc">
						<p class="bloc1">
							Pourquoi nous?
						</p>

						<p class="bloc1">
							<ul style="list-style:none">
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Satisfaction</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Sécurité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Fiabilité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Garantie</span></li>
							</ul>
						</p>
				</div>

				<div class="col-md-3 bloc">
						<p class="bloc1">
							Adresses
						</p>

						<p class="bloc2">
							<ul class=" bloc2" style="list-style:none">
								<li><span class="glyphicon glyphicon-flag icon"></span>&nbsp Pays: &nbsp <span class="p-footer">Cameroun</span></li>

								<li><span class="glyphicon glyphicon-map-marker icon"></span>&nbsp Ville: &nbsp <span class="p-footer">Douala</span></li>

								<li><span class="glyphicon glyphicon-phone icon"></span>&nbsp Téléphone: &nbsp <span class="p-footer">(+237) 676 233 273</span></li>

								<li><span class="glyphicon glyphicon-envelope icon"></span>&nbsp Email:&nbsp <span class="p-footer">info@inchclass.org</span></li>
							</ul>
						</p>
				</div>

				<div class="col-md-3 bloc">
						<p class="bloc1">
							Suivez-Nous
						</p>
						<P class="bloc1">
							<ul style="list-style:none">
								<li><span><i class="fa fa-facebook facebook"></i>&nbsp Facebook: <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-twitter twitter"></i></span>&nbsp Twitter: &nbsp <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-linkedin linkedin"></i></span>&nbsp LinkedIn: &nbsp <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-whatsapp whatsapp"></i></span></span>&nbsp Whatsapp:&nbsp <a href=""><span class="p-footer">(+237)676 233 273</span></a></li>
							</ul>
						</p>
				</div>

				<div class="col-md-3 bloc">
					<div class="row">
						<div class="col-md-12  col-sm-12 col-xs-12">
							<label class="bloc4">Newsletter</label>
							<div class="input-group bloc5">
								<form>	
									<input class="form-control" type="text" name="text" placeholder="Type Your Email Yere" style="font-style:italic; font-family:arial narrow">
									<button class="btn pull pull-right bf" style="background-color:#3595ce; color:white; border:1px solid white; font-family:arial narrow">Subscribe</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<ul class="row2">
					<li>Copyright © 2021 Inch Class</li>
					<li>Powered by Inch Class</li>
				</ul>
			</div>
		</div>
	</div>


	<script type="text/javascript" src="javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="javascript/action.js"></script>
	<script type="text/javascript" src="javascript/action1.js"></script>
	<script type="text/javascript">  // Fonction  désactivation de affichage  « Message  erreur »
        var Error = document.getElementsByClassName('Erreur');
        // var Photo = document.getElementById('Bloc_Photo');
          var Nom = document.getElementById('Nom');

        function desactive() {
          for (var i = 0 ; i < Error.length ; i++) {
              Error[i].style.display = 'none';
          }
          // Photo.style.display = 'none';
        }
        desactive();

      // Nom.onfocus =  function(){
      //  console.log(Nom.value);
      //  if (Nom.value ='') {
      //    Error[0].style.display = 'block';
      //  }else{
      //    console.log(Error[0].value);
      //  }
      // }
  </script>

  <script>
  	var loadFile = function(event) {
        var profil = document.getElementById('pp');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
  </script>
	<!-- <script>
    	var formulaire= getElementById("formulaire");
    	var bouton= getElementById ("affiche");
    	bouton.addEventlistener('click', function()){
        formulaire.style.display= 'block'
    	}
	</script> -->
</body>
</html>